package com.gadomsky.cars;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;


class IdExtractorTest {

    private IdExtractor  idExtractor = new IdExtractor();
    @Test
    void shouldExtractId(){
        assertEquals("ID6EU7I1", idExtractor.extract("https://www.otomoto.pl/oferta/bmw-seria-4-bmw-430i-grand-coupe-xdrive-m-performance-idealny-ID6EU7I1.html"));
    }
}