package com.gadomsky.cars;

import java.io.IOException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.util.List;
import java.util.stream.Collectors;


interface EventsHandler {
    void onEvents(List<OfferEvent> newEvents);
}

class SoutEventsHandler implements EventsHandler {
    @Override
    public void onEvents(List<OfferEvent> newEvents) {
        newEvents.forEach(x -> System.out.println(x.state + ": " + x));
    }
}

class GnomeEventsHandler implements EventsHandler {
    @Override
    public void onEvents(List<OfferEvent> newEvents) {
        ProcessBuilder processBuilder = new ProcessBuilder();

        var changed = newEvents.stream().filter(e -> e.state.equals("BEFORE")).toList().size();
        var added = newEvents.stream().filter(e -> e.state.equals("ADDED")).toList().size();
        var deleted = newEvents.stream().filter(e -> e.state.equals("DELETED")).toList().size();

        try {
            processBuilder.command("bash", "-c", "notify-send 'new car offers' 'changed: " + changed + ", added: " + added + ", deleted: " + deleted + "'")
                    .start();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}

class PushbulletEventsHandler implements EventsHandler {
    @Override
    public void onEvents(List<OfferEvent> newEvents) {
        var changed = newEvents.stream().filter(e -> e.state.equals("BEFORE")).toList().size();
        List<OfferEvent> added = newEvents.stream().filter(e -> e.state.equals("ADDED")).toList();
        var deleted = newEvents.stream().filter(e -> e.state.equals("DELETED")).toList().size();

       var msg = String.format("""
               {"body": "https://gitlab.com/kragg5.ster/otomoto-monitoring/-/commits/main changed:%s, deleted:%s, added:%s : %s", "title": "%s", "type":"note"}""", changed, deleted, added.size(), added.stream().map(e -> e.url).collect(Collectors.joining(" ")), System.getenv("CAR"));

        try {

            var client = HttpClient.newHttpClient();
            HttpRequest request = HttpRequest.newBuilder()
                    .uri(new URI("https://api.pushbullet.com/v2/pushes"))
                    .header("Access-Token", System.getenv("PUSHBULLET_TOKEN"))
                    .header("Content-Type", "application/json")
                    .POST(HttpRequest.BodyPublishers.ofString(msg))
                    .build();

            var resp = client.send(request, HttpResponse.BodyHandlers.ofString());
            System.out.println(resp);
        } catch (Throwable e) {
            throw new RuntimeException(e);
        }
    }
}

public class Notifier {

    static Notifier defaultNotifier() {
        return new Notifier(
                List.of(
                        new SoutEventsHandler(),
                        new PushbulletEventsHandler()
                )
        );
    }

    private final List<EventsHandler> eventsHandlers;

    public Notifier(List<EventsHandler> handlers) {
        this.eventsHandlers = handlers;
    }

    public void notify(List<OfferEvent> newEvents) {
        eventsHandlers.forEach(h -> h.onEvents(newEvents));
    }
}
