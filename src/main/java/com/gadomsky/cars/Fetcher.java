package com.gadomsky.cars;

import org.json.JSONObject;
import org.jsoup.Jsoup;
import org.jsoup.nodes.*;

import java.io.*;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Stream;

interface Fetcher {
    FetchedOffers fetch();
}

class RetryingFetcher implements Fetcher {
    final Fetcher f;

    RetryingFetcher(Fetcher f) {
        this.f = f;
    }

    @Override
    public FetchedOffers fetch() {
        return fetch(3);
    }

    FetchedOffers fetch(int left) {
        try {
            return f.fetch();
        } catch (Throwable t) {
            if (left > 0) {
                return fetch(left - 1);
            } else throw new RuntimeException(t);
        }
    }
}

class DefaultFetcher implements Fetcher {
    final String url;
    IdExtractor idExtractor = new IdExtractor();

    DefaultFetcher(String url) {
        this.url = url;
    }

    @Override
    public FetchedOffers fetch() {
        Document doc = null;
        try {
            doc = Jsoup.connect(url).get();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        var hasNext = doc.getElementsByAttributeValue("title", "Next Page").size() > 0;

        var jsonStr = doc.getElementsByAttributeValue("id", "__NEXT_DATA__").first().childNode(0).toString();

        JSONObject jo = new JSONObject(jsonStr);

        var data = ((JSONObject) ((JSONObject) ((JSONObject) jo
                .get("props"))
                .get("pageProps")).
                get("urqlState")).toMap()
                .values().stream().toList().get(0);

        JSONObject jsonData = new JSONObject(((Map<String, String>) data).get("data"));
        List<Map<String, Object>> adds = ((Map<String, List<Map<String, Map<String, Object>>>>) jsonData.toMap().get("advertSearch"))
                .get("edges")
                .stream()
                .map(m -> m.get("node"))
                .toList();

        var offers = adds.stream().map(ad -> {
            var price = "" + ((Map<String, Map<String, Integer>>) ad.get("price")).get("amount").get("units");
            var title = "" + ad.get("title");
            var link = "" + ad.get("url");
            var shortDesc = "" + ad.get("shortDescription");
            var params = ((List<Map<String, String>>) ad.get("parameters")).stream().map(it -> it.get("displayValue")).toList();
            var id = new OfferId(idExtractor.extract(link));

            return new Offer(id, title, shortDesc, price, link, params);
        }).toList();

        return new FetchedOffers(hasNext, offers);
    }
}

class MultiPageFetcher implements Fetcher {
    final String url;

    MultiPageFetcher(String url) {
        this.url = url;
    }

    @Override
    public FetchedOffers fetch() {
        return fetch(1);
    }

    private FetchedOffers fetch(int page) {
        var res = new RetryingFetcher(new DefaultFetcher(url + "&page=" + page)).fetch();
        if (res.hasNext()) {
            var next = fetch(page + 1);
            return new FetchedOffers(next.hasNext(), Stream.of(res.offers(), next.offers()).flatMap(Collection::stream).toList());
        } else {
            return res;
        }
    }

}

record FetchedOffers(boolean hasNext, List<Offer> offers) {
}

record OfferValues(String url, String title, String titleExtended, String price, Set<String> params) {
}

record Offer(OfferId id, String title, String titleExtended, String price, String url, List<String> params) {
    OfferValues values() {
        return new OfferValues(url, title, titleExtended, price, new HashSet<>(params));
    }

    public OfferSnapshot toSnapshot() {
        return new OfferSnapshot(id.id(), url, paramsAsString(), title, titleExtended, price);
    }

    public String paramsAsString() {
        return String.join("|", params);
    }
}


record OfferId(String id) {
}

class IdExtractor {
    Pattern p = Pattern.compile("(.+-)(.+)(\\.html)");

    String extract(String url) {
        Matcher matcher = p.matcher(url);
        if (matcher.find()) {
            return matcher.group(2);
        }
        return url;
    }
}