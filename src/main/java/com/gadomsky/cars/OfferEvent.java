package com.gadomsky.cars;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;

public class OfferEvent {

    public OfferEvent() {

    }

    public OfferEvent(String id, String url, String params, String title, String titleExtended, String price, String state, String datetime) {
        this.id = id;
        this.url = url;
        this.params = params;
        this.title = title;
        this.titleExtended = titleExtended;
        this.price = price;
        this.state = state;
        this.datetime = datetime;
    }

    String id;
    String url;
    String params;
    String title;
    String titleExtended;
    String price;
    String state; // states: BEFORE, AFTER, ADDED, DELETED
    String datetime;

    static OfferEvent deleted(Offer o) {
        return new OfferEvent(o.id().id(), o.url(), o.paramsAsString(), o.title(), o.titleExtended(), o.price(), "DELETED", now());
    }

    static OfferEvent added(Offer o) {
        return new OfferEvent(o.id().id(), o.url(), o.paramsAsString(), o.title(), o.titleExtended(),o.price(), "ADDED", now());
    }

    static List<OfferEvent> changed(Offer before, Offer after) {
        return List.of(
                new OfferEvent(before.id().id(), before.url(), before.paramsAsString(), before.title(), before.titleExtended(), before.price(), "BEFORE", now()),
                new OfferEvent(after.id().id(), after.url(), after.paramsAsString(), after.title(), after.titleExtended(), after.price(), "AFTER", now())
        );
    }

    private static String now() {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");
        return LocalDateTime.now().format(formatter);
    }

    @Override
    public String toString() {
        return "OfferEvent{" +
                "id='" + id + '\'' +
                ", url='" + url + '\'' +
                ", params='" + params + '\'' +
                ", title='" + title + '\'' +
                ", price='" + price + '\'' +
                ", state='" + state + '\'' +
                ", datetime='" + datetime + '\'' +
                '}';
    }
}
